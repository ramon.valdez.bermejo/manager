import React, { Component } from "react";
//import { View, Text } from "react-native";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import ReduxThunk from "redux-thunk";
import reducers from "./reducers";
import firebase from "firebase";
import Router from "./Router";

class App extends Component {
  componentDidMount() {
    const firebaseConfig = {
      apiKey: "AIzaSyBdDKzR9z7nfXQTTofzcQy3GPlBwwhhmEg",
      authDomain: "manager-55493.firebaseapp.com",
      databaseURL: "https://manager-55493.firebaseio.com",
      projectId: "manager-55493",
      storageBucket: "",
      messagingSenderId: "800720142304",
      appId: "1:800720142304:web:fae56aef84ab69b4"
    };
    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}

export default App;
