import React from "react";
//import { View } from "react-native";
import { Scene, Router, Stack, Actions } from "react-native-router-flux";
import LoginForm from "./components/LoginForm";
import EmployeeList from "./components/EmployeeList";
import EmployeeCreate from "./components/EmployeeCreate";

const RouterComponent = () => {
  return (
    <Router>
      <Stack
        key="root"
        sceneStyle={{ paddingTop: 65 }}
        titleStyle={{ textAlign: "center", flex: 1 }}
        hideNavBar
      >
        <Stack key="auth">
          <Scene
            key="login"
            component={LoginForm}
            title="Please login"
            initial
          />
        </Stack>
        <Stack key="main">
          <Scene
            rightTitle="Add"
            onRight={() => Actions.employeeCreate()}
            key="employeeList"
            component={EmployeeList}
            title="Employees"
          />
          <Scene
            key="employeeCreate"
            component={EmployeeCreate}
            title="Create Employee"
          />
        </Stack>
      </Stack>
    </Router>
  );
};

export default RouterComponent;
