// Import libraries
import React from 'react';
import { Text, View } from 'react-native';

// Create component

const Header = (props) => {
  const { textStyle, viewStyle } = styles;

  return (
    <View style={viewStyle}>
      <Text style={textStyle}>{props.headerText}</Text>
    </View>
  );
};

const styles = {
  viewStyle: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    paddingTop: 10,
    paddingBottom: 10,
    backgroundColor: '#F8F8F8',
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2, //for iOS
    elevation: 10, //for Android
    position: 'relative'
  },
  textStyle: {
    fontSize: 20
  }
};

// Make component available to other parts of the app
export { Header };
