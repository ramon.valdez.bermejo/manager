import { EMPLOYEE_UPDATE } from "../actions/types";

const INITIAL_STATE = {
  name: "",
  phone: "",
  shift: ""
};

export default (state = INITIAL_STATE, action) => {
  console.log(action.type);
  console.log(action.type === EMPLOYEE_UPDATE);
  switch (action.type) {
    case EMPLOYEE_UPDATE:
      console.log(
        "EMPLOYEE_UPDATE: " + action.payload.prop + ":" + action.payload.value
      );
      return { ...state, [action.payload.prop]: action.payload.value};
    default:
      console.log("default");
      return state;
  }
};
